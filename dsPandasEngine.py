import pandas as pd
from pandas import DataFrame
import numpy as np
from jinja2 import Environment, FileSystemLoader
import yaml
import time
from datetime import datetime
import os
import datacompy


def row_counts(df):
    return df.shape[0]

def column_counts(df):
    return df.shape[1]


# Load the configuration
CONFIG_FILE = "test_config.yaml"
config_yaml = open(CONFIG_FILE)
testcases = yaml.load(config_yaml, Loader = yaml.FullLoader)

# Execute the testcase in loop, the testcase will executed as per the confuguration in the yaml file
for testcase in testcases:
    testcase = testcase['testcase']
    print("Executing testcase : " + testcase['name'])

    # Load data files
    SOURCE_CSV_FILE = testcase['data_root'] + os.path.sep + testcase['source']['file_name']
    TARGET_CSV_FILE = testcase['data_root'] + os.path.sep + testcase['target']['file_name']
    # Columns to read
    data_columns = testcase['columns']

    # Loading the Source and Target Dataframes
    sdf = pd.read_csv(SOURCE_CSV_FILE, delimiter=',', usecols=data_columns)
    tdf = pd.read_csv(TARGET_CSV_FILE, delimiter=',', usecols=data_columns)

    # Counting the rows
    sdf_row_count = row_counts(sdf)
    tdf_row_count = row_counts(tdf)
    
    # Counting the columns
    sdf_column_count = column_counts(sdf)
    tdf_column_count = column_counts(tdf)


    # Test printing the values
    # print(sdf_column_count)
    # print(tdf_column_count)
    # print(sdf.shape)

    ### Column Summary

    # Number of columns in common and uncommon:
    common_columns = tdf.columns.intersection(sdf.columns)
    uncommon_columns = tdf.columns.symmetric_difference(sdf.columns)
    # Number of columns in Source but not in Target:
    sdf_columns = list(sdf.head())
    tdf_columns = list(tdf.head())
    column_in_source_unique = list(set(sdf) - set(tdf)) # Columns that are in Source but not in Target
    column_in_target_unique = list(set(tdf) - set(sdf)) # Columns that are in Target but not in Source
    # print(column_in_source_unique) 
    # print(column_in_target_unique) 


    #Rows only in source
    
    df_diff = pd.concat(
        [sdf, tdf],
        axis= 0, 
        join='outer',
        ignore_index= False, 
        keys= ['Source', 'Target'], 
        levels= None, 
        names= None,
        verify_integrity= False, 
        copy=True 
    )
    # .drop_duplicates(keep=False)

    print(df_diff)
    
    

    # df_all = sdf.merge(
    #     tdf.drop_duplicates(), 
    #     on=['Name'], how='left', 
    #     indicator=False)


    
    # print(sdf.keys)



    ### Column compare
    
    



    # Comparind the datasets using configurations from yaml file
    # compare = datacompy.Compare( sdf, tdf,
    #     join_columns = testcase['compare']['join_columns'],    # Name of the column to join on
    #     abs_tol = 0,              #Optional, defaults to 0
    #     rel_tol = 0,              #Optional, default to 0
    #     df1_name = testcase['compare']['df1_name'],      #Optional, name of the source df
    #     df2_name = testcase['compare']['df2_name'],      #Optional, name of the target df
    # )

    # compare.matches(ignore_extra_columns=False)

    # print(compare.report())

    # with open("test_summary_1.txt", 'w') as file:
    #     file.write(compare.report())