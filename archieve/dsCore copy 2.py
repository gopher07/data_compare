import pandas as pd
from pandas import DataFrame
import datacompy
from jinja2 import Environment, FileSystemLoader
# from weasyprint import HTML


def showCount(tag, icount):
    count = icount
    print("The count for " + tag + " : " + str(count))


# Load the configuration


# Source data file
SOURCE_CSV_FILE = 'pokemon_data_source.csv'

# Target data file
TARGET_CSV_FILE = 'pokemon_data_target.csv'

# Columns to read
sdf_fields = ['Name','Type 1','Type 2','HP','Attack','Defense','Sp. Atk','Sp. Def','Speed','Generation']
tdf_fields = ['Name','Type 1','Type 2','HP','Attack','Defense','Sp. Atk','Sp. Def','Speed','Legendary']

# Loading the Source and Target Dataframes
sdf = pd.read_csv(SOURCE_CSV_FILE, delimiter=',', usecols=sdf_fields)
tdf = pd.read_csv(TARGET_CSV_FILE, delimiter=',', usecols=tdf_fields)

# TODO: Need to write a logic to compare the selected columns, or import the selected columns to the DataFrame

# print(sdf)
# print(tdf)

# Source - Counting the number of records
# i_sdf = sdf.index #Counting the index
# showCount( "source", len(i_sdf))

# Target - Counting the number of records
# i_tdf = tdf.index #Counting the index
# showCount( "target", len(i_tdf))

# Comparing the dataframes
# print(DataFrame.eq(sdf,tdf))

# Using datacompy
compare = datacompy.Compare(
    sdf,
    tdf,
    # on_index=True,
    join_columns="Name",    # Name of the column to join on
    abs_tol=0,              #Optional, defaults to 0
    rel_tol=0,              #Optional, default to 0
    df1_name='Source',      #Optional, name of the source df
    df2_name='Target',      #Optional, name of the target df
)

compare.matches(ignore_extra_columns=False)
# print(compare.report())

all_mismatch = compare.all_mismatch() # All the  mismatchig columns
source_unique_columns = compare.df1_unq_columns()  #Get columns that are unique to df1
target_unique_columns = compare.df2_unq_columns()  #Get columns that are unique to df2

print(source_unique_columns)

# Setting the html template
pd.set_option('colheader_justify', 'center')
html_template = '''
<html>
  <head><title>HTML Pandas Dataframe with CSS</title></head>
  <link rel="stylesheet" type="text/css" href="df_style.css"/>
  <script src="report_decorator.js"></script>
  <body>
    <div> 
      <div> <h2> Total comparison datasheet <h2> </div>
      <div> {mismatch_table}   </div>
    </div>
    <br/>
    <div> 
      <div> <h2> Total comparison datasheet <h2> </div>
      <div> 
        <table border="1" class="dataframe mystyle">
          <thead>
            <tr style="text-align: center;">
              <th>Source</th>
              <th>Target</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{source_unique_column_values}</td>
              <td>{target_unique_columns}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div> </div>
  </body>
</html>.
'''

with open('test_overall_data_report.html', 'w') as f:
    f.write(html_template.format(mismatch_table=all_mismatch.to_html(classes='mystyle')))
    f.write(html_template.format(source_unique_column_values=source_unique_columns))









