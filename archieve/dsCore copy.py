import pandas as pd
from pandas import DataFrame
import datacompy
from jinja2 import Environment, FileSystemLoader
# from weasyprint import HTML


def showCount(tag, icount):
    count = icount
    print("The count for " + tag + " : " + str(count))


# Load the configuration


# Source data file
SOURCE_CSV_FILE = 'pokemon_data_source.csv'

# Target data file
TARGET_CSV_FILE = 'pokemon_data_target.csv'

# Columns to read
fields = ['Name','Type 1','Type 2','HP','Attack','Defense','Sp. Atk','Sp. Def','Speed','Generation','Legendary']

# Loading the Source and Target Dataframes
sdf = pd.read_csv(SOURCE_CSV_FILE, delimiter=',', usecols=fields)
tdf = pd.read_csv(TARGET_CSV_FILE, delimiter=',', usecols=fields)

# TODO: Need to write a logic to compare the selected columns, or import the selected columns to the DataFrame

# print(sdf)
# print(tdf)

# Source - Counting the number of records
# i_sdf = sdf.index #Counting the index
# showCount( "source", len(i_sdf))

# Target - Counting the number of records
# i_tdf = tdf.index #Counting the index
# showCount( "target", len(i_tdf))

# Comparing the dataframes
# print(DataFrame.eq(sdf,tdf))

# Using datacompy
compare = datacompy.Compare(
    sdf,
    tdf,
    # on_index=True,
    join_columns="Name",    # Name of the column to join on
    abs_tol=0,              #Optional, defaults to 0
    rel_tol=0,              #Optional, default to 0
    df1_name='Source',      #Optional, name of the source df
    df2_name='Target',      #Optional, name of the target df
)

compare.matches(ignore_extra_columns=False)
print(compare.report())

all_mismatch = compare.all_mismatch()
# print(all_mismatch)

# Setting the html template
pd.set_option('colheader_justify', 'center')
html_template = '''
<html>
  <head><title>HTML Pandas Dataframe with CSS</title></head>
  <link rel="stylesheet" type="text/css" href="df_style.css"/>
  <script src="report_decorator.js"></script>
  <body>
    {table}
  </body>
</html>.
'''

with open('test_overall_data_report.html', 'w') as f:
    f.write(html_template.format(table=all_mismatch.to_html(classes='mystyle')))

# with open('test_summary_report.html', 'w') as f: 
#     f.write(html_template.format(table=compare.report().to_html(classes='mystyle'))) #This is not working as the compare.report does not have a to_html function


# df_html = DataFrame.to_html(all_mismatch)
# print(all_mismatch.to_html)




# env = Environment(loader=FileSystemLoader('.'))
# template = env.get_template("myreport.html")

# template_vars = { "title": "Data comparison - Sample Report", "my_pivot_table": comparison_report.to_html() }
# template_vars = { "title": "Data comparison - Sample Report", "my_pivot_table": all_mismatch.to_html() }
# html_out = template.render((template_vars))


# Conversion to PDF
# HTML(string=html_out).write_pdf('test_report.pdf')








