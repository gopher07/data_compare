import pandas as pd
from pandas import DataFrame
import datacompy
from jinja2 import Environment, FileSystemLoader
import yaml
import time
from datetime import datetime
import os
    
def showCount(icount):
    count = icount
    print("The counnt is: " + count)

def create_test_results_folder(test_results_folder):
    try: 
        os.mkdir(test_results_folder) #All the test resuls files would be created in this folder
    except OSError:
        print('Could not create new directory, It might be existing.' )
    else:
        print('Created new directory for test results.')

def execute_testcase(testcase):
    print("Executing testcase : " + testcase['name'])
  
    # Load data files
    SOURCE_CSV_FILE = testcase['data_root'] + os.path.sep + testcase['source']['file_name']
    TARGET_CSV_FILE = testcase['data_root'] + os.path.sep + testcase['target']['file_name']
    # Columns to read
    data_columns = testcase['columns']
    
    # Loading the Source and Target Dataframes
    sdf = pd.read_csv(SOURCE_CSV_FILE, delimiter=',', usecols=data_columns)
    tdf = pd.read_csv(TARGET_CSV_FILE, delimiter=',', usecols=data_columns)

    # Comparind the datasets using configurations from yaml file
    compare = datacompy.Compare( sdf, tdf,
        join_columns = testcase['compare']['join_columns'],    # Name of the column to join on
        abs_tol = testcase['compare']['abs_tol'],              #Optional, defaults to 0
        rel_tol = testcase['compare']['rel_tol'],              #Optional, default to 0
        df1_name = testcase['compare']['df1_name'],      #Optional, name of the source df
        df2_name = testcase['compare']['df2_name'],      #Optional, name of the target df
    )

    compare.matches(ignore_extra_columns=False)

    execution_summary = compare.report(sample_count=testcase['sample_count'])

    all_mismatch = compare.all_mismatch() # All the  mismatchig columns

    # Setting the html template
    pd.set_option('colheader_justify', 'center')
    dt_string = datetime.now().strftime("%Y_%m%d_%H%M%S") #Formatting the data and time

    # df_html = DataFrame.to_html(all_mismatch)
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template(testcase['report_template_file']) #Loading the html template here


    df_diff = pd.concat(
        [sdf, tdf],
        axis= testcase['merge_axis'], 
        join='outer',
        ignore_index= False, 
        keys= ['Source', 'Target'], 
        levels= None, 
        names= None,
        verify_integrity= False, 
        copy=True 
    )



    # Passing the variables to the template
    template_vars = { 
        "heading": testcase['name'], 
        "total_rows_source" : sdf.shape[0],
        "total_rows_target" : tdf.shape[0],
        "total_columns_source" : sdf.shape[1],
        "total_columns_target" : tdf.shape[1],
        "row_matches": compare.count_matching_rows(),
        "do_columns_match": compare.all_columns_match(),
        "do_rows_match": compare.all_rows_overlap(),
        "timestamp": dt_string , 
        "unique_rows_source": compare.df1_unq_rows.to_html(table_id="unique_rows_source_table", classes='datatable'),
        "unique_rows_source_shape": compare.df1_unq_rows.shape,
        "unique_rows_target": compare.df2_unq_rows.to_html(table_id="unique_rows_target_table", classes='datatable'),
        "unique_rows_target_shape": compare.df2_unq_rows.shape,
        "data_table": all_mismatch.to_html(table_id="mismatch_table", classes='datatable'),
        'df_shape': all_mismatch.shape,
        "all_data_table": df_diff.to_html(table_id="all_data_table", classes='datatable'),
        'all_data_table_shape': df_diff.shape,
    } 
    output = template.render((template_vars))
    
    
    test_mismatch_result_name = str(testcase['name']) + '_' + str(dt_string) + '_mismatch_results.html'
    test_summary_result_name = str(testcase['name']) + '_' + str(dt_string) +'_execution_summary.txt'
    test_results_folder = 'test_results'
    create_test_results_folder(test_results_folder)

    #  Creating the html for mismatch records
    with open(test_results_folder + os.path.sep + test_mismatch_result_name, 'w') as file:
        file.write(output)
        print('Successfully written the mismatch output to html')
    
    # Creating the text file for summary
    with open(test_results_folder + os.path.sep + test_summary_result_name, 'w') as file:
        file.write(execution_summary)
        print('Successfully written the summary output to doc')
    