import pandas as pd
import re

CSV_FILE = 'pokemon_data_source.csv'
df = pd.read_csv(CSV_FILE, delimiter=',')

# print(df.head(3))
# print(df.tail(3))

# Reading Headers
# print(df.columns)

# Get one pertinumar columns with top 5
# print(df['Name'][0:5])
# print(df.Name[0:5])

#Get multiple columns
# print(df[['Name', 'Type 1']][0:5])



# print each row
# print(df.iloc[1])

# Loopin through the rows
# for index, row in df.iterrows():
#     print(index, row)

#loc is like iloc but it give more flexibility in term of text
#Find a row based on the text
# df.loc[df['Type 1'] == 'Fire']

# Describe gives us some defualt set of calculations
# print(df.describe()['HP'])

# Sorting in pandas
# print(df.sort_values(['Type 1', 'HP' ], ascending=[1,0]))

# Add a custom column
# df['Total'] = df['HP']+ df['Attack'] + df['Sp. Atk']
# print(df.head(5))

# Drop a columnm, Dropping a column does not drop a coumn direclty but you have to 
# df = df.drop(columns=['Total'])
# df['Total']1
# print(df.head(5))

# Filter a column
# print(df.loc[(df['Type 1'] == 'Grass') | (df['Type 2'] == 'Poison' ) & df['Attack'] > 100])

# Filter records based on chars
# print(df.loc[df['Name'].str.contains('Mega')])
# Just the reverse of the above line
# print(df.loc[~df['Name'].str.contains('Mega')])

# We can also passin the regex
# print(df.loc[df['Type 1'].str.contains('Fire|Grass', regex= True)])

# If you want the case sensitivety off on regex, use the keyword below i.e. flags=re.I
# print(df.loc[df['Type 1'].str.contains('fire|grass', flags=re.I, regex= True)])
# print(df.loc[df['Name'].str.contains('^pi[a-z]*', flags=re.I, regex= True)])


# Reindexing the dataframe
# new_df = df.reset_index()
# # Drop the old index
# new_df = df.reset_index(drop=True)

# # Drop the old index in the same dataframe
# df = df.reset_index(drop=True, inplace=True)



# Conditional changes in the df, in the below statement it will search for for Fire in Column and make changes in Type 2 column
# df.loc[df['Type 1'] == 'Fire', 'Type 2'] = 'Flamer'
# print(df)

# # Statistics
# print(df.groupby(['Type 1']).mean().sort_values('Defense', ascending=False))
# print(df.groupby(['Type 1']).sum().sort_values('Defense', ascending=False))
# print(df.groupby(['Type 1']).count().sort_values('Defense', ascending=False))

# Load data in chunks
for df in pd.read_csv(CSV_FILE, chunksize=5):
    print("CHUNK")
    print(df)

