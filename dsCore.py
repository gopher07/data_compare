import yaml
from dsCommon import execute_testcase

# Load the configuration
CONFIG_FILE = "test_config.yaml"
config_yaml = open(CONFIG_FILE)
testcases = yaml.load(config_yaml, Loader = yaml.FullLoader)

# Execute the testcase in loop, the testcase will executed as per the confuguration in the yaml file
for testcase in testcases:
  execute_testcase(testcase['testcase'])













